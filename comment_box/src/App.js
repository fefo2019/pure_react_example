import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <div className="App-intro">
          <CommentBox />
        </div>
      </div>
    );
  }
}

class CommentBox extends React.Component{

  constructor(){
    super();
    this.state = {
      showComments: false,
      comments: [
          { id: 1, author:'Morgan Freeman', body:'holololo' },
          { id: 2, author:'Tom Cruise', body:'tan tan tan' }
        ]
    };
  }

  render(){
    const comments = this.getComments();
    let commentNodes;
    let buttonText = 'Show comments';
    if(this.state.showComments){
      buttonText = 'Hide comments';
      commentNodes = <div className="comment-list"> {comments} </div>
    }

    return(
      <div className="comment-box">
        <CommentForm addComment={this.addComment.bind(this)}/>
        <h3>COMMENTS</h3>
        <button className="btn btn-large" onClick={this.handleClick.bind(this)}> {buttonText}  </button>
        <h4 className="comment-count"> {this.getCommentsTitle(comments.length)} </h4>
        {commentNodes}
      </div>
    );
  }

  getComments(){
    return this.state.comments.map( (comment) => {
      return (<Comment author={comment.author} body={comment.body} key={comment.id} />)
    });
  }

  getCommentsTitle(commentsCount){
    if(commentsCount === 0){
      return 'No comments yet'
    }else if(commentsCount === 1){
      return '1 comment'
    }else{
      return commentsCount + ' comments';
    }
  }

  addComment(author, body){
    const comment = {
      id: this.state.comments.length + 1,
      author,
      body
    }
    this.setState({comments: this.state.comments.concat([comment])});
  }

  handleClick(){
    this.setState({
      showComments: !this.state.showComments
    });
  }
}

class Comment extends React.Component{
  render(){
    return(
      <div className="comment">
        <div className="comment-header">
          <p> {this.props.author}</p>
          <a href="#" className="comment-footer-delete"> DELETE COMMENT </a>
        </div>
        <p className="comment-body"> {this.props.body} </p>
      </div>
    );
  }
}

class CommentForm extends React.Component{
  render(){
    return(
      <form className="comment-form" onSubmit={this.handleSubmit.bind(this  )}>
        <h3> Join the discussion </h3>
        <div className="comment-form-fields">
          <input placeholder="name" ref={(input) => this.author = input}/>
          <textarea placeholder="comment" ref={(textarea) => this.body = textarea} ></textarea>
        </div>
        <div className="comment-form-actions">
          <button type="submit">
            Post Comment
          </button>
        </div>
      </form>
    )
  }

  handleSubmit(event){
    event.preventDefault();

    let author = this.author;
    let body = this.body;

    this.props.addComment(author.value, body.value);
  }
}

export default App;
